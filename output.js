var fs = require('fs');
module.exports = fileName => {
  fileName = `${fileName}.json`;
  const writeStream = fs.createWriteStream(fileName);
  const writeData = data => writeStream.write(data, 'utf8');
  const close = () => writeStream.end();
  return { writeData, close, };
}