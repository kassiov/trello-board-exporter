require('dotenv').load();
const output = require('./output');
const axios = require('axios').default;
const { API_KEY, API_TOKEN } = process.env;
const boardNotFoundMsg = 'Quadro Inexistente';
const LIMIT = 1000;

const API = {
  baseURL: 'https://api.trello.com/1/',
  actionsRoute: 'actions/',
  boardsRoute: 'boards/',
  cardsRoute: 'cards/',
  checklistsRoute: 'checklists/',
  customFieldsRoute: 'customFields/',
  labelsRoute: 'labels/',
  listsRoute: 'lists/',
  membersMeRoute: 'members/me/',
  membersRoute: 'members/',
}

const reducer = (acc, cur, idx) => idx > 2 ? `${acc} ${cur}` : `${cur}`;
const boardNameFromArgs = (() => process.argv.reduce(reducer))();

const addAuthParams = url => `${url}key=${API_KEY}&token=${API_TOKEN}`;
const addLimitParam = url => `${url}limit=${LIMIT}&`;

const members = async () => {
  let url = API.baseURL;
  url = `${url}${API.membersMeRoute}?`;
  url = addAuthParams(url);
  try {
    const resp = await axios.get(url);
    return resp.data;
  } catch(error) {
    throw new Error(error.response.data);
  }
}

const boards = async (boardId, all=false) => {
  let url = API.baseURL;
  url = `${url}${API.boardsRoute}${boardId}?`;
  all ? url = `${url}fields=all&` : url = url;
  url = addAuthParams(url);
  try {
    const resp = await axios.get(url);
    return resp.data;
  } catch { return }
}

const getBoardInfo = async (idBoards, boardName) => {
  const results = idBoards.map(async idBoard => {
    let b = await boards(idBoard, true);
    if(boardName === b.name) return b;
  });
  const board = await Promise.all(results)
    .then(boards => boards.filter(b => b)[0]);
  if(board) return board;
  else throw new Error(boardNotFoundMsg);
}

const getBoardField = async (boardId, fieldRoute) => {
  let url = API.baseURL;
  url = `${url}${API.boardsRoute}${boardId}/${fieldRoute}/?`;
  url = addLimitParam(url);
  url = addAuthParams(url);
  try {
    const resp = await axios.get(url);
    return resp.data;
  } catch(error) {
    throw new Error(error.response.data);
  }
}

const exportBoard = (fileName, board) => {
  const out = output(fileName);
  out.writeData(JSON.stringify(board));
  out.close();
}

const main = async boardName => {
  try {
    const member = await members();
    const idBoards = member.idBoards;
    let board = await getBoardInfo(idBoards, boardName);
    board.actions = await getBoardField(board.id, API.actionsRoute);
    board.cards = await getBoardField(board.id, API.cardsRoute);
    board.labels = await getBoardField(board.id, API.labelsRoute);
    board.lists = await getBoardField(board.id, API.listsRoute);
    board.members = await getBoardField(board.id, API.membersRoute);
    board.checklists = await getBoardField(board.id, API.checklistsRoute);
    board.customFields = await getBoardField(board.id, API.customFieldsRoute);
    exportBoard(boardName, board);
  } catch(error) {
    console.log(error);
  }
}

process.argv.length < 3 ?
  console.log('Digite o nome do quadro!') :
  main(boardNameFromArgs);