# Configuração de ambiente

Instalar o [node](https://www.nodejs.org) e o [npm](https://www.npmjs.com).

Após baixar o projeto, entre na pasta raiz e execute o comando:

```bash
npm install
```

Crie um arquivo **.env** na raiz do projeto seguindo o modelo do arquivo **.env.example**.

Faça login na sua conta do [trello](https://www.trello.com).

Após fazer o login, entre no endereço [https://trello.com/app-key](https://trello.com/app-key) para adquirir sua chave de api. Substitua a chave adquirida no arquivo **.env**.

Agora use a seguinte url:

https://trello.com/1/authorize?expiration=never&name=MyPersonalToken&scope=read&response_type=token&key={YourAPIKey}

Substitua *MyPesonalToken* pelo nome do seu usuário no trello.

Substitua *{YourAPIKey}* pela chave que você já adquiriu.

Após as substituições, cole essa url na barra de navegação do seu navegador. Ao carregar a página, clique em *Permitir*.

Agora você tem o seu token. Substitua o token adquirido no arquivo **.env**.

Está tudo pronto. Dentro do diretório do projeto, execute o comando:

```bash
npm start <nome do quadro no trello>
```

O programa exporta o quadro indicado e salva em um arquivo **nome do quadro no trello.json**.